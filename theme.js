var theme = {
    content: '#content',

    error: '.error',
    errorElement: 'label',
    errorTextInput: {show: function(input) {
            $(input).parents('.form-line').addClass('error');
        }, hide: function(input) {
            $(input).parents('.form-line').removeClass('error')
        },
    },

    ajax: {
        onPageLoad: {begin: function () {
                $('#content-loader-wrapper').fadeIn();
            }, end: function () {
                $('#content-loader-wrapper').fadeOut();
            },
        },
        onModalLoad: {show: function () {
                $('[ajaxModal]').modal('show');
            }, hide: function () {
                $('.modal-backdrop').fadeOut(function () {
                    $('.modal-backdrop').remove();
                });
                $('#content-loader-wrapper').fadeIn();
                $('[ajaxModal]').modal('hide');
            }, close: function () {
                $('.modal-backdrop').fadeOut(function () {
                    $('.modal-backdrop').remove();
                });
                $('[ajaxModal]').modal('hide');
            },
        },
    },
};
