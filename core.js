var core = {};

/*
 |   Theme conf #BEGIN
 -----------------------------------------------------------*/

core.theme = theme;

/*-----------------------------------------------------------
 |   Theme conf #END
 */

/*
 |   Parsers #BEGIN
 -----------------------------------------------------------*/

core.findInEventPath = function (path, objType) {
    var retValue = null;
    for(var key in path)
    {
        var item = path[key];
        if(item.localName == objType) {
            retValue = item;
            break;
        }
    }
    return retValue;
};

core.ajaxValidate = function (form) {

    var selector = '#'+ form.id +' '+ core.theme.errorElement +  core.theme.error;

    //Clear all labels
    $(selector).html('');

    for(var error in form.errors) {
        var elemSelector = selector +'[for='+error+']';
        var elem = $(elemSelector);
        elem.html(form.errors[error][0]);
    }

    core.highlightErrors(form);
};

/*-----------------------------------------------------------
 |   Parsers #END
 */


/*
 |   view #BEGIN
 -----------------------------------------------------------*/

core.highlightErrors = function (form) {
    var formElements = $(form).serializeArray();

    for(var key in formElements) {
        var elem = formElements[key];
        var name = elem.name;
        var selector = '#' + form.id+ ' [name='+name+']';
        elem = $(selector)[0];

        switch (elem.type) {
            case 'text':
            case 'password' :
                if(typeof form.errors[name] !== 'undefined') {
                    core.theme.errorTextInput.show(elem);
                } else {
                    core.theme.errorTextInput.hide(elem);
                }
                break;
        }
    }
};

/*-----------------------------------------------------------
 |   view #END
 */

/*
|   Ajax #BEGIN
-----------------------------------------------------------*/

core.ajaxSubmit = function () {
    var e = event;
    var form = core.findInEventPath(e.path, 'form');

    if(form == null) {
        var error = new SyntaxError();
        error.message = 'form not found';
        throw error;
    }

    form.$data = $(form).serialize();
    $.ajax({
        url: form.action,
        type: form.method,
        data: form.$data,
        error: function (jqXHR, textStatus, errorThrown) {
            if(typeof jqXHR.responseJSON.errors !== undefined) {
                var errors = jqXHR.responseJSON.errors;
                form.errors = errors;

                console.dir(form);

                core.ajaxValidate(form);
            }
        }

    });
};

core.ajaxParseLoader = function (link, func) {
    link.onclick = function () { return false };

    link.method = $(link).attr("method");
    if ( link.method === undefined || link.method === false) {
        link.method = 'post';
    }

    link.action = $(link).attr("action");
    if ( link.action === undefined || link.action === false) {
        var error = new SyntaxError();
        error.message = 'attribute \'action\' not found';
        throw error;
    }

    func(link.action, link.method, '');
};

core.ajaxLoadPage = function (action, method, data) {
    core.theme.ajax.onPageLoad.begin();
    $.ajax({
        url: action,
        type: method,
        async: true,
        success: function(data, textStatus, jqXHR){
            $(core.theme.content).html(data);
            window.history.pushState("", "", action);

        },

    });
    core.theme.ajax.onPageLoad.end();
};

core.ajaxLoadModal = function (action, method, data) {
    $.ajax({
        url: action,
        type: method,
        async: true,
        success: function(data, textStatus, jqXHR){
            $( "body" ).append( data );
            core.theme.ajax.onModalLoad.show();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
};

/*-----------------------------------------------------------
|   Ajax #END
*/

/*
 |  Core init #BEGIN
 -----------------------------------------------------------*/

core.reloadEventListeners = function () {
    $('[ajaxSubmit]').unbind('click');
    $('[ajaxSubmit]').on("click", function () {
        core.ajaxSubmit();
    });

    $('[ajaxLoadPage]').unbind('click');
    $('[ajaxLoadPage]').on("click", function () {
        core.theme.ajax.onPageLoad.begin();
        core.ajaxParseLoader(this, core.ajaxLoadPage);
    });

    $('[ajaxLoadModal]').unbind('click');
    $('[ajaxLoadModal]').on("click", function () {
        core.ajaxParseLoader(this, core.ajaxLoadModal);
    });
    $('[closeAjaxModal]').on("click", function () {
        core.theme.ajax.onModalLoad.close();
        $('[ajaxModal]').remove();
    });
};

core.init = function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $( document ).ajaxSuccess(function( event, request, settings ) {
        var data = request.responseJSON;
        if(typeof data !== 'undefined') {

            data.redirect = request.responseJSON['redirect'];
            data.script = request.responseJSON['script'];

            if (typeof data.redirect !== 'undefined') {
                $(location).attr("href", data.redirect);
            }
            if (typeof data.script !== 'undefined') {
                eval(data.script);
            }
            if (typeof data.ajaxLoadPage !== 'undefined') {
                core.ajaxLoadPage(data.ajaxLoadPage.url, data.ajaxLoadPage.type, '');
            }
        }
    });

    core.reloadEventListeners();
};

$(document).ready(function() {
    core.init();
});

/*-----------------------------------------------------------
 |   Core init #END
 */
